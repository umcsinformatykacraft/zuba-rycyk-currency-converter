package implementation;

import api.Currency;
import api.ExchangeRateService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ExchangeRateServiceMock implements ExchangeRateService {
	private Map<Currency, Set<AmountSet>> amounts;

	public ExchangeRateServiceMock() {
		// tymczasowe zamockowanie danych
		amounts = new HashMap<>();
		amounts.put(Currency.EUR, new HashSet<>());
		amounts.get(Currency.EUR).add(new AmountSet(LocalDateTime.of(2019,1,1,0,1),
				LocalDateTime.of(2019,2,1,0,1),
				new BigDecimal(2)));
		amounts.get(Currency.EUR).add(new AmountSet(LocalDateTime.of(2019,2,1,0,1),
				LocalDateTime.of(2019,3,1,0,1),
				new BigDecimal(3.99)));

		amounts.put(Currency.USD, new HashSet<>());
		amounts.get(Currency.USD).add(new AmountSet(LocalDateTime.of(2019,1,1,0,1),
				LocalDateTime.of(2019,3,1,0,1),
				new BigDecimal(3.43)));
	}

	public BigDecimal baseToAnother(Currency currency, LocalDateTime date) {
		Set<AmountSet> amountSets = amounts.get(currency);
		for (AmountSet amountSet : amountSets){
			if(date.isAfter(amountSet.from) && date.isBefore(amountSet.to)) {
				return amountSet.predictor;
			}
		}
		throw new RuntimeException("Not find predictor");
	}

	public BigDecimal anotherToBase(Currency currency, LocalDateTime date) {
		Set<AmountSet> amountSets = amounts.get(currency);
		for (AmountSet amountSet : amountSets){
			if(date.isAfter(amountSet.from) && date.isBefore(amountSet.to)) {
				return BigDecimal.ONE.divide(amountSet.predictor, 2, RoundingMode.HALF_UP);
			}
		}
		throw new RuntimeException("Not find predictor");
	}

	class AmountSet {
		public LocalDateTime from;
		public LocalDateTime to;
		public BigDecimal predictor;

		public AmountSet(LocalDateTime from, LocalDateTime to, BigDecimal predictor) {
			this.from = from;
			this.to = to;
			this.predictor = predictor;
		}
	}
}
