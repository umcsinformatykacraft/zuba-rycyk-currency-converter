package implementation;

import api.Currency;
import api.CurrencyConverter;
import api.ExchangeRateService;
import api.Money;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;

public class CurrencyConverterImpl implements CurrencyConverter {
	private Currency base;
	private ExchangeRateService exchangeRateService;
	private MathContext TWO = new MathContext(2, RoundingMode.HALF_EVEN);

	public CurrencyConverterImpl(ExchangeRateService exchangeRateService, Currency baseCurrency) {
		this.exchangeRateService = exchangeRateService;
		this.base = baseCurrency;
	}

	public Money convert(Money money, Currency currency, LocalDateTime date) {
		Currency from = money.getCurrency();
		Currency to = currency;

		if (from.equals(base)) {
			return convertFromBase(new Money(money.getAmount(), currency), date);
		} else if (to.equals(base)) {
			return convertToBase(money, date);
		} else {
			Money base = convertToBase(money, date);
			return convertFromBase(new Money(base.getAmount(), to), date);
		}
	}

	private Money convertToBase(Money money, LocalDateTime date) {
		BigDecimal predictor = exchangeRateService.anotherToBase(money.getCurrency(), date);
		return money.multiply(predictor, base);
	}

	private Money convertFromBase(Money money, LocalDateTime date) {
		BigDecimal predictor = exchangeRateService.baseToAnother(money.getCurrency(), date);
		return  money.multiply(predictor, money.getCurrency());
	}
}
