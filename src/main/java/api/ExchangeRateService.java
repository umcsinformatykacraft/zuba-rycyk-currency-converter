package api;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface ExchangeRateService {
	BigDecimal baseToAnother (Currency currency, LocalDateTime date);
	BigDecimal anotherToBase (Currency currency, LocalDateTime date);
}
