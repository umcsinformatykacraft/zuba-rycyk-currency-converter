package api;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Money {
	public static final BigDecimal ZERO = BigDecimal.ZERO;

	private BigDecimal amount;
	private Currency currency;

	public Money(BigDecimal amount, Currency currency) {
		this.amount = amount;
		this.currency = currency;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public Money multiply (BigDecimal predictor, Currency currency) {
		return new Money(amount.multiply(predictor).setScale(2, RoundingMode.HALF_UP), currency);
	}

	@Override
	public String toString() {
		return "Money{" +
				"amount=" + amount +
				", currency=" + currency +
				'}';
	}
}
