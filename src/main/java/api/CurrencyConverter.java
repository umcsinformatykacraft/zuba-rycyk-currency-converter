package api;

import java.time.LocalDateTime;

public interface CurrencyConverter {
	Money convert (Money money, Currency currency, LocalDateTime date);
}
