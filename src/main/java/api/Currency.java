package api;

import java.util.Objects;

public class Currency {
	public static final Currency PLN = new Currency("PLN");
	public static final Currency EUR = new Currency("EUR");
	public static final Currency USD = new Currency("USD");

	private String currencyName;

	public Currency(String currencyName) {
		this.currencyName = currencyName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Currency currency = (Currency) o;
		return Objects.equals(currencyName, currency.currencyName);
	}

	@Override
	public int hashCode() {

		return Objects.hash(currencyName);
	}

	@Override
	public String toString() {
		return currencyName;
	}
}
