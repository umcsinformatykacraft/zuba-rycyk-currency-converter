package implementation;

import api.Currency;
import api.CurrencyConverter;
import api.ExchangeRateService;
import api.Money;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class CurrencyConverterImplTest {
	public CurrencyConverter currencyConverter;
	@Before
	public void setUp() throws Exception {
		ExchangeRateService exchangeRateService = new ExchangeRateServiceMock();
		currencyConverter = new CurrencyConverterImpl(exchangeRateService, Currency.PLN);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void baseToEur() throws Exception {
		Money onePln = new Money(new BigDecimal(1), Currency.PLN);
		Money converted = currencyConverter.convert(onePln, Currency.EUR, LocalDateTime.of(2019, 1, 2, 0, 1));
		assertEquals(converted.getCurrency(), Currency.EUR);
		assertEquals(converted.getAmount().compareTo(new BigDecimal(2)), 0);
	}

	@Test
	public void eurToBase() throws Exception {
		Money onePln = new Money(new BigDecimal(1), Currency.EUR);
		Money converted = currencyConverter.convert(onePln, Currency.PLN, LocalDateTime.of(2019, 1, 2, 0, 1));
		assertEquals(converted.getCurrency(), Currency.PLN);
		assertEquals(converted.getAmount().compareTo(new BigDecimal(0.50)), 0);

	}

	@Test
	public void usdToEur() throws Exception {
		Money onePln = new Money(new BigDecimal(1), Currency.USD);
		Money converted = currencyConverter.convert(onePln, Currency.EUR, LocalDateTime.of(2019, 1, 2, 0, 1));
		assertEquals(converted.getCurrency(), Currency.EUR);
		assertEquals(converted.getAmount().compareTo(new BigDecimal(0.58).setScale(2, RoundingMode.HALF_UP)), 0);
	}
}